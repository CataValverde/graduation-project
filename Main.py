import tkinter as tk
import tkinter.messagebox as tkmessagebox

from MyClasses import *
import random



class Application(tk.Frame):
    
    def __init__(self, master=None):
        self.ButtonWidth=20
        self.ButtonHeight=5

        self.Categories=ListCategories()
        try:
            self.Categories.Load()
        except:
            self.Categories.Save()

        tk.Frame.__init__(self, master)
        self.Master=master
        self.CurrentFrame=tk.Frame(self.Master)
        
        self.pack()

        self.MainScreen()


    def MultipleLabel(self, TextLabels,X,Y):
        pos=0
        for text in TextLabels:
            tk.Label(self.CurrentFrame, text=text,width=self.ButtonWidth,height=self.ButtonHeight, 
                bg="black",fg="white").grid(row=Y[pos],column=X[pos])
            pos+=1
            

    def CleaningFrame(self):
        self.CurrentFrame.destroy()
        self.CurrentFrame=tk.Frame(self.Master)
        self.Master.configure(bg="black")
        self.CurrentFrame.configure(bg="black")
        self.CurrentFrame.pack()

    #---------------------------------------
    #.............Main Screen...............

    def MainScreen(self):
        self.CleaningFrame()

        TextLabels=["","",""]
        X=[1,1,1]
        Y=[1,3,5]
        self.MultipleLabel(TextLabels,X,Y)

        bt1=tk.Button(self.CurrentFrame,text="Random Generator",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="red",
         command=lambda:self.RandomScreen(None,None,self.MainScreen))
        bt1.grid(row=2,column=1)


        bt2=tk.Button(self.CurrentFrame,text="Categories",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="blue",
            command=self.CategoriesScreen)
        bt2.grid(row=4,column=1)

        bt3=tk.Button(self.CurrentFrame,text="Create New Category",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="green",
            command=self.AddCategoryScreen)
        bt3.grid(row=6,column=1)




    #---------------------------------------
    #.............Random Screen...............
    def RandomScreen(self, Category, Subject, ReturnFunction):
        self.CleaningFrame()
        #pickingCategoru
        if Category==None:
            Category=random.choice(self.Categories.Categories)

            Counter=0
            while len(self.Categories.Categories[Category.ListPosition].Subsections)==0:
                Category=random.choice(self.Categories.Categories)
                Counter+=1
                if Counter>1000:    #Securitie break
                    break

        if Subject==None:
            Subject=random.choice(self.Categories.Categories[Category.ListPosition].Subsections)
            Counter=0
            while len(self.Categories.Categories[Category.ListPosition].Subsections[Subject.ListPosition].ExercisesDictio)==0:
                Subject=random.choice(self.Categories.Categories[Category.ListPosition].Subsections)
                Counter+=1
                if Counter>1000:    #Securitie break
                    break
              
        DictKeys=self.Categories.DictionaryGetKeys( Category.ListPosition,Subject.ListPosition)
        ListOfKeys=[]
        for k in DictKeys:
            ListOfKeys.append(k)


        Exercise=random.choice(ListOfKeys)
        Counter=0
        while self.Categories.DictionaryGetValue(Category.ListPosition,Subject.ListPosition,Exercise)==1:
            Exercise=random.choice(self.Categories.Categories[Category.ListPosition].Subsections[Subject.ListPosition].ExercisesDictio)
            Counter+=1
            if Counter>1000:    #Securitie break
                break


            

        CategoryName,Name,Book, Page=self.Categories.GetSubjectInformation( Category.ListPosition, Subject.ListPosition)
        TextLabels=["Ramdom Exercise","Category : ",CategoryName,"Subject : ",Name, "Book : ", Book, "Page : ",Page, "Exercise", Exercise ]
        X=[1,1,2,1,2,1,2,1,2,1,2]
        Y=[1,2,2,3,3,4,4,5,5,6,6]
        self.MultipleLabel(TextLabels,X,Y)



        bt1=tk.Button(self.CurrentFrame,text="Back",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="red",
         command=ReturnFunction)
        bt1.grid(row=7,column=1)

        bt2=tk.Button(self.CurrentFrame,text="Exercise Done",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="green",
         command=lambda:self.ExerciseDone(Category.ListPosition, Subject.ListPosition,Exercise,ReturnFunction))
        bt2.grid(row=7,column=2)


    def ExerciseDone(self, CategoryNumber,SubjectNumber, key, ReturnFunction):
        self.Categories.ChangeKeyValue( CategoryNumber,SubjectNumber,key)
        ReturnFunction()
        
        
        



    #---------------------------------------
    #.............Add Categpry Screen...............

    def AddCategoryScreen(self):
        self.CleaningFrame()
        TextLabels=["Category Name",""]
        X=[2,2]
        Y=[1,3]
        self.MultipleLabel(TextLabels,X,Y)

        TextEntry= tk.Entry(self.CurrentFrame)
        TextEntry.grid(row=2,column=2)

        bt1=tk.Button(self.CurrentFrame,text="Back",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="red",
         command=self.MainScreen)
        bt1.grid(row=4,column=1)

        bt2=tk.Button(self.CurrentFrame,text="Save",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="blue",
             command=lambda:self.SaveCategory(TextEntry.get()))
        bt2.grid(row=4,column=3)


    def SaveCategory(self, Text):
        self.Categories.AddCategory(Text)
        self.MainScreen()



    #---------------------------------------
    #.............Categories Screen...............
    def CategoriesScreen(self):
        self.CleaningFrame()

        X=1
        Y=2
        ListOfButtons=[]
        for Category in self.Categories.Categories:
            ListOfButtons.append(tk.Button(self.CurrentFrame,text=Category.Name,fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="blue",
                command= lambda Position=Category.ListPosition:self.SubjectScreen(Position)))

        for bt in ListOfButtons:
            bt.grid(row=Y,column=X)
            X+=1
            if X>5:
                X=1
                Y+=1


        # Back Button
        bt1=tk.Button(self.CurrentFrame,text="Back",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="red",
         command=self.MainScreen)
        bt1.grid(row=10,column=3)


        self.MultipleLabel(["Categories","","","",""],[3,1,2,4,5],[0,10,10,10,10])







#---------------------------------------
    #.............Subject Screen...............

    def SubjectScreen(self, CategoryNumber):
        self.CleaningFrame()

        X=1
        Y=2
        for Subject in self.Categories.Categories[CategoryNumber].Subsections:
            bt=tk.Button(self.CurrentFrame,text=Subject.Name,fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="blue",
                command= lambda Position=Subject.ListPosition : self.ExercisesScreen(CategoryNumber,Position))
            bt.grid(row=Y,column=X)
            X+=1
            if X>5:
                X=1
                Y+=1


        #Back Button
        bt1=tk.Button(self.CurrentFrame,text="Back",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="blue",
         command=self.CategoriesScreen)
        bt1.grid(row=10,column=1)

        #Delete Button
        bt2=tk.Button(self.CurrentFrame,text="Delete Category",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="red",
         command=lambda:self.DeleteCategory(CategoryNumber))
        bt2.grid(row=10,column=3)

        #Add Subject button
        bt3=tk.Button(self.CurrentFrame,text="Add Subject",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="green",
         command=lambda:self.AddSubjectScreen(CategoryNumber))
        bt3.grid(row=10,column=5)

        #Ramdom From Category
        

        bt4=tk.Button(self.CurrentFrame,text="Select Ramdom Exercise",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="green",
         command=lambda:self.RandomScreen(self.Categories.Categories[CategoryNumber], None, lambda:self.SubjectScreen(CategoryNumber)))
        bt4.grid(row=0,column=5)

        self.MultipleLabel([self.Categories.Categories[CategoryNumber].Name,"",""],[3,2,4],[0,10,10])


    def DeleteCategory(self, CategoryNumber):
        if tkmessagebox.askyesno(title=None, message="Are you sure you want to delete category "+self.Categories.Categories[CategoryNumber].Name+"?")==True:
            self.Categories.DeleteCategory(CategoryNumber)
            self.MainScreen()



#---------------------------------------
    #.............Add Subject Screen...............

    def AddSubjectScreen(self, CategoryNumber):
        self.CleaningFrame()

        Texts=["Subject Name", "Book", "Page", "Initial Exercise Number", "Final Exercise Number"]
        Y=[1,2,3,4,5]
        X=[1,1,1,1,1]
        self.MultipleLabel(Texts,X,Y)

        TextName= tk.Entry(self.CurrentFrame)
        TextName.grid(row=1,column=2)

        TextBook= tk.Entry(self.CurrentFrame)
        TextBook.grid(row=2,column=2)

        TextPage= tk.Entry(self.CurrentFrame)
        TextPage.grid(row=3,column=2)

        TextInitial= tk.Entry(self.CurrentFrame)
        TextInitial.grid(row=4,column=2)

        TextFinal= tk.Entry(self.CurrentFrame)
        TextFinal.grid(row=5,column=2)


        #Back BUtton
        bt1=tk.Button(self.CurrentFrame,text="Back",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="blue",
         command=lambda:self.SubjectScreen( CategoryNumber))
        bt1.grid(row=10,column=1)

        #Save Button
        bt2=tk.Button(self.CurrentFrame,text="Save",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="green",
         command=lambda:self.SaveSubject( CategoryNumber,TextName.get(),TextBook.get(), TextPage.get(),TextInitial.get(),TextFinal.get() ))
        bt2.grid(row=10,column=2)

    def SaveSubject(self, CategoryPosition, Name, Book, Page, InitialExerNum, FinalExerNum):
        #Checking Errors
        Error=False

        #EmptyName
        if Name=="":
            tkmessagebox.showerror(title="Error", message="Name blank is empty")
            Error=True
        #Empty Book
        if Book=="":
            tkmessagebox.showerror(title="Error", message="Book blank is empty")
            Error=True

        #Empty Page
        if Page=="":
            tkmessagebox.showerror(title="Error", message="Page blank is empty")
            Error=True
        #Empty Page
        if InitialExerNum=="":
            tkmessagebox.showerror(title="Error", message="Initial Exercise Number is empty")
            Error=True
        #Empty Page
        if FinalExerNum=="":
            tkmessagebox.showerror(title="Error", message="Final Exercise Number blank is empty")
            Error=True

     
        try:
            numA=int(InitialExerNum)

            try:
                numB=int(FinalExerNum)

                   #Empty Inital bigger than Final
                if numA>numB:
                    tkmessagebox.showerror(title="Error", message="Initial Exercise Number is bigger than Final Exercise Number")
                    Error=True

            except:
                tkmessagebox.showerror(title="Error", message="Final Exercise Number is not a number")
                Error=True

        except:
            tkmessagebox.showerror(title="Error", message="Initial Exercise Number is not a number")
            Error=True


        


        if Error==False:
            self.Categories.AddSubject(CategoryPosition, Name, Book, Page, numA, numB)
            self.SubjectScreen( CategoryPosition)



#---------------------------------------
    #.............Exercises Screen...............

    def ExercisesScreen(self, CategoryNumber, SubjectNumber):
        self.CleaningFrame()

        CategoryName,Name,Book,Page=self.Categories.GetSubjectInformation(CategoryNumber, SubjectNumber)
        Texts=["Category",CategoryName, "Subject",Name, "Book",Book, "Page",Page,"","","Green buttons=Done"]
        Y=[1,1,1,1,2,2,2,2,10,10,1]
        X=[1,2,3,4,1,2,3,4,1,2,5]
        self.MultipleLabel(Texts,X,Y)

        X=1
        Y=3
     
        
        for Key in self.Categories.DictionaryGetKeys(CategoryNumber,SubjectNumber):  
            Value=self.Categories.DictionaryGetValue(CategoryNumber,SubjectNumber,Key)

            if Value==True:
                c1 = tk.Button(self.CurrentFrame, text=str(Key),
                    bg="green", fg="white", command=lambda MyKey=Key:self.ChangeValue(c1,CategoryNumber,SubjectNumber,MyKey))
            else:

                c1 = tk.Button(self.CurrentFrame, text=str(Key),
                 bg="blue", fg="white", command=lambda MyKey=Key:self.ChangeValue(c1,CategoryNumber,SubjectNumber,MyKey))

            c1.grid(row=Y,column=X)
            
            
            X+=1
            if X>5:
                X=1
                Y+=1



        #Back Button
        bt1=tk.Button(self.CurrentFrame,text="Back",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="blue",
         command=lambda:self.SubjectScreen(CategoryNumber))
        bt1.grid(row=11,column=1)

         #Delete Subject Button
        bt1=tk.Button(self.CurrentFrame,text="Delete Subject",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="red",
         command=lambda:self.DeleteSubjectFromCategory(CategoryNumber,SubjectNumber))
        bt1.grid(row=11,column=5)

        #Random from Subject Button
        bt1=tk.Button(self.CurrentFrame,text="Random Exercise",fg="white",width=self.ButtonWidth,height=self.ButtonHeight,bg="green",
         command=lambda:self.RandomScreen( self.Categories.Categories[CategoryNumber], 
            self.Categories.Categories[CategoryNumber].Subsections[SubjectNumber], lambda:self.ExercisesScreen(CategoryNumber, SubjectNumber)))
        bt1.grid(row=1,column=5)



    def ChangeValue(self,CategoryNumber,SubjectNumber,Key):        
        self.Categories.ChangeKeyValue(CategoryNumber,SubjectNumber,Key)
        self.ExercisesScreen(CategoryNumber, SubjectNumber)

    def DeleteSubjectFromCategory(self, CategoryPosition,SubjectPosition):
        NameCat,NameSub,NameBook,page,=self.Categories.GetSubjectInformation( CategoryPosition, SubjectPosition)
        if tkmessagebox.askyesno(title=None, 
            message="Are you sure you want to delete Subject "+NameSub+" from category "+ NameCat+"?")==True:
            self.Categories.DeleteSubject(CategoryPosition, SubjectPosition)
            self.SubjectScreen(CategoryPosition)










        

root = tk.Tk()
root.title("Random Exercises Generator")
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
root.geometry(str(screen_width)+"x"+str(screen_height))
app = Application(master=root)
app.mainloop()
