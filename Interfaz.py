from tkinter import *
from tkinter import messagebox
from tkinter import ttk
import csv
import random
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg)
from matplotlib.figure import Figure
from tkinter import filedialog
import sched, time
import threading 
import sys

import RPi.GPIO as GPIO

from ina219 import INA219


GPIO.setmode(GPIO.BCM)

class Halogeno():
    def __init__(self, Estado):

        self.Estado = Estado

    def LuzAlta(self):
        if self.Estado == "Apagado":
            self.Estado = "Luz Alta"



    def LuzBaja(self):
        if self.Estado == "Luz Alta":
            self.Estado = "Luz Baja"


    def Apagado(self):
        if self.Estado == "Luz Baja":
            self.Estado = "Apagado"


class App(Frame):

    def __init__(self, master = None):

        Frame.__init__(self, master)
        self.Master=master
        self.CurrentFrame=Frame(self.Master)
        self.pack()

        self.ButtonWidth =20
        self.ButtonHeight = 4
        self.BT = 4
        self.BL = 2
        self.DT = 15
        self.DL = 2

        self.Halogenos = []

        for i in range(0,12):
            self.Halogenos.append(Halogeno("Apagado"))

        self.Encendido = GPIO.HIGH
        self.Apagado = GPIO.LOW

        self.pinesLuzBaja = [4, 17, 27, 22, 10, 9, 11, 5, 6, 13, 19, 26]
        self.pinesLuzAlta = [14, 15, 18, 23, 24, 25, 8, 7, 12, 16, 20, 21]

        for j in self.pinesLuzAlta:
            
            GPIO.setup(j, GPIO.OUT)
            GPIO.output(j, self.Apagado)

        for k in self.pinesLuzBaja:
            
            GPIO.setup(k, GPIO.OUT)
            GPIO.output(k, self.Apagado)

        self.SHUNT_OHMS = 0.1
        self.MAX_EXPECTED_AMPS = 0.2

        self.ina = INA219(self.SHUNT_OHMS, self.MAX_EXPECTED_AMPS)
        self.ina.configure(self.ina.RANGE_16V)

        self.voltage= str(round(self.ina.voltage(), 2))
        self.Corriente = str(round(self.ina.current(), 2))

        self.menu()
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    def CleaningFrame(self):

        self.CurrentFrame.destroy()
        self.CurrentFrame=Frame(self.Master)
        self.Master.configure(bg="#A9A9A9")
        self.CurrentFrame.configure(bg="#A9A9A9")
        self.CurrentFrame.pack()

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    def salir(self):
        sys.exit()
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    def menu(self):
        self.CleaningFrame()

        Button(self.CurrentFrame, text = "Control del modo de \n operación de los \n halógenos", bg = "#1E90FF", font = 16,
            command = self.Control, width = self.ButtonWidth, height = self.ButtonHeight).grid(row = 0, column = 0, padx = 5, pady = 5)

        Button(self.CurrentFrame, text = "Recolección de datos", bg = "#FF4500", font = 16,
            command = self.RecoleccionDeDatos, width = self.ButtonWidth, height = self.ButtonHeight).grid(row = 0, column = 1, padx = 5, pady = 5)

        Button(self.CurrentFrame, text = "Despliegue de datos", bg = "#32CD32", font = 16,
            command = self.DespliegueDeDatos, width = self.ButtonWidth, height = self.ButtonHeight).grid(row = 1, column = 0, padx = 5, pady = 5)

        Button(self.CurrentFrame, text = "Graficar datos \n recolectados", bg = "#DC143C", font = 16,
            command = self.graficarDatos, width = self.ButtonWidth, height = self.ButtonHeight).grid(row = 1, column = 1, padx = 5, pady =5)

        Button(self.CurrentFrame, text = "Salir", width = self.BT, height = self.BL, font = 16,
            command = self.salir).grid(row = 2, column = 3, padx = 5, pady = 5)

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    def Control(self):

        self.CleaningFrame()

        Label(self.CurrentFrame, text = "Código de colores para la identificación \n del estado de los halógenos",
            bg = "#A9A9A9", font = 16).grid(row = 0, column = 5, padx = 10, pady = 10)
        Label(self.CurrentFrame, text = "Halógeno en \n luz alta", bg = "red", relief = "sunken",
            height = 2, width = 12, font = 16).grid(row = 1, column = 5, padx = 10, pady = 10)
        Label(self.CurrentFrame,text = "Halógeno en \n luz baja", bg = "yellow", relief = "sunken",
            height = 2, width = 12, font = 16).grid(row = 2, column = 5, padx = 10, pady = 10)
        Label(self.CurrentFrame,text = "Halógeno \n apagado", bg = "#696969", relief = "sunken",
            height = 2, width = 12, font = 16).grid(row = 3, column = 5, padx = 10, pady = 10)
        Label(self.CurrentFrame,text = "Presione uno de los botones a \n la izquierda para cambiar el estado \n de los halógenos", 
            bg = "#A9A9A9", font = 16).grid(row = 4, column = 5, padx = 10, pady = 10)


        x = 0
        y = 0
    
        for i in self.Halogenos:

            self.comprobarEstado(i, x, y)
            x = x + 1
            if x == 3:
                x = 0
                y = y + 1
        Button(self.CurrentFrame, text = "Menu", width = self.BT, height = self.BL,font = 16,
            command = self.menu).grid(row = 7,column = 6, padx = 5, pady = 5)

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    def comprobarEstado(self, halogeno, x, y):
        
        if halogeno.Estado == "Apagado":

            Button(self.CurrentFrame, text = "H"+str(self.Halogenos.index(halogeno)+1), bg = "#696969", font = 16,
                command = lambda: self.cambiarALuzAlta(halogeno), width = self.BT, 
                height = self.BL).grid(row = y, column = x, padx = 5, pady = 5)
            

            
        elif halogeno.Estado == "Luz Alta":

            Button(self.CurrentFrame, text = "H"+str(self.Halogenos.index(halogeno)+1), bg = "red", font = 16,
                command = lambda: self.cambiarALuzBaja(halogeno), width = self.BT, 
                height = self.BL).grid(row = y, column = x, padx = 5, pady = 5)
            


        elif halogeno.Estado == "Luz Baja":

            Button(self.CurrentFrame, text = "H"+str(self.Halogenos.index(halogeno)+1), bg = "yellow",font = 16,
                command = lambda: self.cambiarAApagado(halogeno), width = self.BT, 
                height = self.BL).grid(row = y, column = x, padx = 5, pady = 5)
            

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    def cambiarALuzAlta(self, halogeno):

        halogeno.LuzAlta()
        GPIO.output(self.pinesLuzAlta[self.Halogenos.index(halogeno)], self.Encendido)
        GPIO.output(self.pinesLuzBaja[self.Halogenos.index(halogeno)], self.Apagado)
        self.Control()


    def cambiarALuzBaja(self, halogeno):
        
        halogeno.LuzBaja()
        GPIO.output(self.pinesLuzBaja[self.Halogenos.index(halogeno)], self.Encendido)
        GPIO.output(self.pinesLuzAlta[self.Halogenos.index(halogeno)], self.Apagado)
        self.Control()


    def cambiarAApagado(self, halogeno):
        
        halogeno.Apagado()
        GPIO.output(self.pinesLuzBaja[self.Halogenos.index(halogeno)], self.Apagado)
        GPIO.output(self.pinesLuzAlta[self.Halogenos.index(halogeno)], self.Apagado)
        self.Control()

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    def DespliegueDeDatos(self):
        self.CleaningFrame()

        Tiempo = Button(self.CurrentFrame, text = "Tiempo", bg = "#800080", fg = "white", width = self.DT, font = 16,
            height = self.DL).grid(row = 0, column = 0, padx = 5, pady = 5)
        DatosTiempo = Label(self.CurrentFrame, text = str(), width = self.DT, height = self.DL, font = 16).grid(row = 1, 
            column = 0, padx = 5, pady = 5)

        Voltaje = Button(self.CurrentFrame, text = "Voltaje [V]", bg = "blue", fg = "white", width = self.DT, font = 16,
            height = self.DL).grid(row = 0, column = 1, padx = 5, pady = 5)
        DatosVoltaje = Label(self.CurrentFrame, text = self.voltage, width = self.DT, height = self.DL, font = 16).grid(row = 1, 
            column = 1, padx = 5, pady = 5)


        Corriente = Button(self.CurrentFrame, text = "Corriente [mA]", bg = "green", fg = "white", width = self.DT, font = 16,
            height = self.DL).grid(row = 0, column = 2, padx = 5, pady = 5)
        DatosCorriente = Label(self.CurrentFrame, text = self.corriente, width = self.DT, height = self.DL, font = 16).grid(row = 1, 
            column = 2, padx = 5, pady = 5)

        Irradiancia = Button(self.CurrentFrame, text = "Irradiancia", bg = "yellow", width = self.DT, font = 16,
            height = self.DL).grid(row = 2, column = 0, padx = 5, pady = 5)
        DatosIrradiancia = Label(self.CurrentFrame, width = self.DT, height = self.DL, font = 16).grid(row = 3, 
            column = 0, padx = 5, pady = 5)


        Temperatura = Button(self.CurrentFrame, text = "Temperatura", bg = "red", fg = "white", width = self.DT, font = 16,
            height = self.DL).grid(row = 2, column = 2, padx = 5, pady = 5)
        DatosTemperatura = Label(self.CurrentFrame, width = self.DT, height = self.DL, font = 16).grid(row = 3, 
            column = 2, padx = 5, pady = 5)

        Regresar = Button(self.CurrentFrame, text = "Menu", command = self.menu, width = self.BT,
            height = self.BL, font = 16).grid(row = 4, column = 1, padx = 5, pady = 5)
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    def RecoleccionDeDatos(self):
        self.CleaningFrame()

        tiempoDeRecoleccion = Label(self.CurrentFrame, text = "Tiempo \n de recolección", bg = "#A9A9A9", font = 16).grid(row = 0, 
            column = 0, padx = 5, pady = 5)

        tiempoDeMuestreo = Label(self.CurrentFrame, text = "Tiempo \n de muestreo", bg = "#A9A9A9", font = 16).grid(row = 1, 
            column = 0, padx = 5, pady = 5)


        t = StringVar()
        tiempo = Entry(self.CurrentFrame, textvariable  = t).grid(row = 0, column = 1, padx = 0, pady = 5)

        m = StringVar()
        muestreo = Entry(self.CurrentFrame, textvariable = m).grid(row = 1, column = 1, padx = 5, pady = 5)

        u = StringVar()
        unidadesTiempo = ttk.Combobox(self.CurrentFrame, values = ["segundos", "minutos", "horas"], 
            textvariable = u).grid(row = 0, column = 2, padx = 5, pady = 5)

        p = StringVar()
        unidadesMuestreo = ttk.Combobox(self.CurrentFrame, values = ["segundos", "minutos", "horas"], 
            textvariable = p).grid(row = 1, column = 2, padx = 5, pady = 5)



        GuardarDatos = Button(self.CurrentFrame, text = "Guardar datos", width = self.DT, height = self.DL, bg = "#000080", fg = "white",
            font = 16, command = lambda: self.guardarDatos(t.get(), m.get(), u.get(), p.get())).grid(row = 2, 
            column = 1, padx = 5, pady = 5)

        Regresar = Button(self.CurrentFrame, text = "Menu", command = self.menu, width = self.BT,
            height = self.BL, font = 16).grid(row = 2, column = 2, padx = 5, pady = 5)

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    def guardarDatos(self, t, m, u, p):


        if t.isdigit() == True and m.isdigit() == True:

            if u == "segundos":

                if p == "segundos":

                    self.eventoAnexarFilas(int(t), int(m))

                elif p == "minutos":

                    self.eventoAnexarFilas(int(t), int(m)*60)

                elif p == "horas":

                    self.eventoAnexarFilas(int(t), int(m)*60*60)

            
            elif u == "minutos":

                if p == "segundos":

                    self.eventoAnexarFilas(int(t)*60, int(m))

                elif p == "minutos":

                    self.eventoAnexarFilas(int(t)*60, int(m)*60)

                elif p == "horas":

                    self.eventoAnexarFilas(int(t)*60, int(m)*60*60)



            elif u == "horas":

                if p == "segundos":

                    self.eventoAnexarFilas(int(t)*60*60, int(m))

                elif p == "minutos":

                    self.eventoAnexarFilas(int(t)*60*60, int(m)*60)

                elif p == "horas":

                    self.eventoAnexarFilas(int(t)*60*60, int(m)*60*60)


        else:
            messagebox.showerror(title = None, message = "Ingrese solo números enteros")
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    def llamarScheduler(self, t, m, filepath):
        self.controlDeRefrescamiento = True
        

        self.s = sched.scheduler(time.time, time.sleep)

        datos1 = ["H1 en "+self.Halogenos[0].Estado, "H2 en "+self.Halogenos[1].Estado, "H3 en "+self.Halogenos[2].Estado, "H4 en "+self.Halogenos[3].Estado,"H5 en "+ self.Halogenos[4].Estado,
        "H6 en "+ self.Halogenos[5].Estado, "H7 en "+self.Halogenos[6].Estado, "H8 en "+ self.Halogenos[7].Estado, "H9 en "+self.Halogenos[8].Estado, "H10 en "+ self.Halogenos[9].Estado,
        "H11 en "+self.Halogenos[10].Estado, "H12 en "+self.Halogenos[11].Estado]
        datos2 = ["Tiempo", "Voltaje", "Corriente", "Irradiancia", "Temperatura"]

        with open(str(filepath), "w") as f:
            writer = csv.writer(f)
            writer.writerow(datos1)
            f.close()
        with open(str(filepath), 'a') as f:
            writer = csv.writer(f)
            writer.writerow(datos2)
            f.close()

        a = 0
        while a <= (t/m):
            self.s.enter(m*a, 1, self.anexarFilas, argument = (filepath,) )
            a = a + 1

        self.s.run()

        self.controlDeRefrescamiento =False

    def eventoAnexarFilas(self, t, m):

        self.controlDeRefrescamiento = True

        filepath = filedialog.asksaveasfilename()

        hilo = threading.Thread(target = self.llamarScheduler, args = (t, m, filepath))
        hilo.start()
        self.mostrarDatosEnRecoleccion(filepath, m)





    def anexarFilas(self, filepath):


        datosActuales = [random.randint(10,100), random.randint(10,100), random.randint(10,100), 
        random.randint(10,100), random.randint(10,100)]

        with open(str(filepath), 'a') as file:
            writer = csv.writer(file)
            writer.writerow(datosActuales)
            file.close()



#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    def mostrarDatosEnRecoleccion(self, filepath,m):
        self.CleaningFrame()

        self.tiem = [0]
        self.volt = [0]
        self.corr = [0]
        self.irra = [0]
        self.temp = [0]

        self.varTiempo = StringVar()
        self.varTiempo.set(0)
        self.varVoltaje = StringVar()
        self.varVoltaje.set(0)
        self.varCorriente = StringVar() 
        self.varCorriente.set(0)
        self.varIrradiancia = StringVar()   
        self.varIrradiancia.set(0)
        self.varTemperatura = StringVar()
        self.varTemperatura.set(0)



        Ltiempo2 = Label(self.CurrentFrame, text = "Tiempo ", bg = "cyan", font = 16, height = self.DL, width = self.DT).grid(row = 0, 
            column = 0, padx = 5, pady = 5 )

        LV2 = Label(self.CurrentFrame, text = "Voltaje ", bg = "blue", fg = "white", font = 16, height = self.DL, width = self.DT).grid(row = 0, 
            column = 1, padx = 5, pady = 5 )

        LC2 = Label(self.CurrentFrame, text = "Corriente ", bg = "green", fg = "white", font = 16, height = self.DL, width = self.DT).grid(row = 0, 
            column = 2, padx = 5, pady = 5)

        LI2 = Label(self.CurrentFrame, text = "Irradiancia ", bg = "yellow", font = 16, height = self.DL, width = self.DT).grid(row = 2, 
            column = 0, padx = 5, pady = 5)

        LT2 = Label(self.CurrentFrame, text = "Temperatura ", bg = "red", fg = "white", font = 16, height = self.DL, width = self.DT).grid(row = 2, 
            column = 2, padx = 5, pady = 5)

        Ltiempo = Label(self.CurrentFrame, textvariable = self.varTiempo, font = 16, height = self.DL, width = self.DT)
        Ltiempo.grid(row = 1, column = 0, padx = 5, pady = 5 )

        LV = Label(self.CurrentFrame, textvariable =self.varVoltaje, font = 16, height = self.DL, width = self.DT)
        LV.grid(row = 1, column = 1, padx = 5, pady = 5 )

        LC = Label(self.CurrentFrame, textvariable = self.varCorriente, font = 16, height = self.DL, width = self.DT)
        LC.grid(row = 1, column = 2, padx = 5, pady = 5)

        LI = Label(self.CurrentFrame, textvariable = self.varIrradiancia, font = 16, height = self.DL, width = self.DT)
        LI.grid(row = 3, column = 0, padx = 5, pady = 5)

        LT = Label(self.CurrentFrame, textvariable = self.varTemperatura, font = 16, height = self.DL, width = self.DT)
        LT.grid(row = 3, column = 2, padx = 5, pady = 5)

        DetenerRecoleccion = Button(self.CurrentFrame, text = "Detener recolección", bg = "red", fg = "white", height = self.DL, width = self.DT,
            command = self.Detencion, font = 16).grid(row = 5, column = 1, padx = 5, pady = 5)



        self.refrescarEtiqueta(Ltiempo, LV, LC, LI, LT, m, filepath)
        


        
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        
    def Detencion(self):
        list = self.s.queue
        for i in list:
            self.s.cancel(i)
        self.menu()


#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    def refrescarEtiqueta(self, Lt, v, c, i, t, m, filepath ):

        with open(str(filepath)) as file:
            reader = csv.reader(file,  delimiter=',', quotechar=',',
                        quoting=csv.QUOTE_MINIMAL)
            for row in reader:
                self.tiem.append(row[0])
                self.volt.append(row[1])
                self.corr.append(row[2])
                self.irra.append(row[3])
                self.temp.append(row[4])

        self.varTiempo.set(self.tiem[-1])
        self.varVoltaje.set(self.volt[-1])
        self.varCorriente.set(self.corr[-1])
        self.varIrradiancia.set(self.irra[-1])
        self.varTemperatura.set(self.temp[-1])

        if self.controlDeRefrescamiento == True:
            Lt.after(m*1000, lambda: self.refrescarEtiqueta(Lt, v, c, i, t, m, filepath))
            v.after(m*1000, lambda: self.refrescarEtiqueta(Lt, v, c, i, t, m, filepath))
            c.after(m*1000, lambda: self.refrescarEtiqueta(Lt, v, c, i, t, m, filepath))
            i.after(m*1000, lambda: self.refrescarEtiqueta(Lt, v, c, i, t, m, filepath))
            t.after(m*1000, lambda: self.refrescarEtiqueta(Lt, v, c, i, t, m, filepath))


#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    def graficarDatos(self):
        self.CleaningFrame()


        Bt = Button(self.CurrentFrame, text = "Graficar datos", width = self.DT, height = self.DL, bg = "#32CD32",  font = 16,
            command = self.graficar).grid(row = 0, column = 0, padx = 5, pady = 5)

        Regresar = Button(self.CurrentFrame, text = "Menu", command = self.menu, width = self.BT, font = 16,
            height = self.BL).grid(row = 0, column = 1, padx = 5, pady = 5)

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    def graficar(self):

        filepath = filedialog.askopenfilename()

        fig = Figure(figsize=(5, 4), dpi=100)

        volt = []
        corr = []
        with open(str(filepath)) as File:
            reader = csv.reader(File, delimiter=',', quotechar=',',
                        quoting=csv.QUOTE_MINIMAL)
            for row in reader:
                volt.append(row[1])
                corr.append(row[2])

        volt.pop(0)
        corr.pop(0)
        volt.pop(0)
        corr.pop(0)

        fig.add_subplot(111).plot(volt, corr)
        canvas = FigureCanvasTkAgg(fig, self.CurrentFrame)
        canvas.draw()
        canvas.get_tk_widget().grid(row = 0, column = 2)

 





#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


root = Tk()
root.title("Sistema de adquisición de datos de un panel solar por medio de un simulador solar")
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
root.geometry(str(screen_width)+"x"+str(screen_height))
app = App(master=root)
app.mainloop()
