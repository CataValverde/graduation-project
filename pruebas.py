
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import random
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg)
from matplotlib.figure import Figure

from tkinter import *




root = Tk()
root.title("Sistema de adquisición de datos de un panel solar por medio de un simulador solar")
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
root.geometry(str(screen_width)+"x"+str(screen_height))


fig = Figure(figsize=(5,5), dpi=100)
a = fig.add_subplot(111)
xs = []
ys = []

def animate(i, xs, ys):

	xs.append(random.randint(10, 100))
	ys.append(random.randint(10, 100))

	xs = xs[-20:]
	ys = ys[-20:]

	a.clear()
	a.plot(xs, ys)


canvas = FigureCanvasTkAgg(fig, root)
canvas.draw()
canvas.get_tk_widget().grid(row = 0, column = 0, padx = 5, pady = 5)

ani = animation.FuncAnimation(fig, animate, fargs=(xs, ys), interval=1000)




root.mainloop()