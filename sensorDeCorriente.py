'''
You'll need to install the Adafruit_Blinka library that provides the CircuitPython support in Python. 
This may also require enabling I2C on your platform and verifying you are running Python 3. 
Since each platform is a little different, and Linux changes often, please visit the CircuitPython on Linux guide to get your 
computer ready!

Once that's done, from your command line run the following command:

    sudo pip3 install adafruit-circuitpython-ina219

'''

import board
import busio
import adafruit_ina219



i2c = busio.I2C(board.SCL, board.SDA)

sensor = adafruit_ina219.INA219(i2c)

print(sensor.bus_voltage)
print(sensor.shunt_voltage)
print(sensor.current)



'''
You'll need to install the Adafruit_Blinka library that provides the CircuitPython support in Python. 
This may also require enabling I2C on your platform and verifying you are running Python 3. 
Since each platform is a little different, and Linux changes often, please visit the CircuitPython on Linux guide to get your 
computer ready!

Once that's done, from your command line run the following command:

    sudo pip3 install adafruit-circuitpython-ads1x15
'''


import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn


i2c = busio.I2C(board.SCL, board.SDA)

ads = ADS.ADS1115(i2c)

ads.gain = 16

#Single ended mode

chan = AnalogIn(ads, ADS.P0)

print(chan.value, chan.voltage)

#Differential mode
#chan = AnalogIn(ads, ADS.P0, ADS.P1)
#print(chan.value, chan.voltage)
#