#Este programa es el control de los halógenos en el simulador de sombras
#para el proyecto de graduación
	
import RPi.GPIO as GPIO
from time import sleep
GPIO.setmode(GPIO.BCM) # GPIO Numbers instead of board numbers


#Se define una variable para cada estado de relé

Encendido = GPIO.HIGH
Apagado = GPIO.LOW

Deco1_A = 17
Deco1_B = 27
Deco1_C = 22
Deco1_D = 5

Deco2_A = 6
Deco2_B = 26
Deco2_C = 16
Deco2_D = 25

Deco3_A = 24
Deco3_B = 23
Deco3_C = 2
Deco3_D = 3

F1 = 4
F2 = 13
F3 = 19



def estadoInicial():

    GPIO.output(Deco1_A, Apagado)
    GPIO.output(Deco1_B, Apagado)
    GPIO.output(Deco1_C, Apagado)
    GPIO.output(Deco1_D, Apagado)
    GPIO.output(Deco2_A, Apagado)
    GPIO.output(Deco2_B, Apagado)
    GPIO.output(Deco2_C, Apagado)
    GPIO.output(Deco2_D, Apagado)
    GPIO.output(Deco3_A, Apagado)
    GPIO.output(Deco3_B, Apagado)
    GPIO.output(Deco3_C, Apagado)
    GPIO.output(Deco3_D, Apagado)
    #Fuente 1 siempre esta encendida
    GPIO.output(F1, Encendido)
    GPIO.output(F2, Apagado)
    GPIO.output(F3, Apagado)


def pinesSalida():
# Se le asigna a los pines el modo de salida
    GPIO.setup(Deco1_A , GPIO.OUT)
    GPIO.setup(Deco1_B , GPIO.OUT)
    GPIO.setup(Deco1_C , GPIO.OUT)
    GPIO.setup(Deco1_D , GPIO.OUT)
    GPIO.setup(Deco2_A , GPIO.OUT)
    GPIO.setup(Deco2_B , GPIO.OUT)
    GPIO.setup(Deco2_C , GPIO.OUT)
    GPIO.setup(Deco2_D , GPIO.OUT)
    GPIO.setup(Deco3_A , GPIO.OUT)
    GPIO.setup(Deco3_B , GPIO.OUT)
    GPIO.setup(Deco3_C , GPIO.OUT)
    GPIO.setup(Deco3_D , GPIO.OUT)
    GPIO.setup(F1 , GPIO.OUT)
    GPIO.setup(F2 , GPIO.OUT)
    GPIO.setup(F3 , GPIO.OUT)



def H1enLuzAltaEncendido():
    GPIO.output(Deco1_A, Apagado)
    GPIO.output(Deco1_B, Apagado)
    GPIO.output(Deco1_C, Apagado)
    GPIO.output(Deco1_D, Apagado)

def H1enLuzAltaApagado():
    GPIO.output(Deco1_A, Apagado)
    GPIO.output(Deco1_B, Apagado)
    GPIO.output(Deco1_C, Apagado)
    GPIO.output(Deco1_D, Encendido)

def H1enLuzBajaEncendido():
    GPIO.output(Deco1_A, Apagado)
    GPIO.output(Deco1_B, Apagado)
    GPIO.output(Deco1_C, Encendido)
    GPIO.output(Deco1_D, Apagado)

def H1enLuzBajaApagado():
    GPIO.output(Deco1_A, Apagado)
    GPIO.output(Deco1_B, Apagado)
    GPIO.output(Deco1_C, Encendido)
    GPIO.output(Deco1_D, Encendido)

#-------------------------------------------

def H2enLuzAltaEncendido():
    GPIO.output(Deco1_A, Apagado)
    GPIO.output(Deco1_B, Encendido)
    GPIO.output(Deco1_C, Apagado)
    GPIO.output(Deco1_D, Apagado)


def H2enLuzAltaApagado():
    GPIO.output(Deco1_A, Apagado)
    GPIO.output(Deco1_B, Encendido)
    GPIO.output(Deco1_C, Apagado)
    GPIO.output(Deco1_D, Encendido)

def H2enLuzBajaEncendido():
    GPIO.output(Deco1_A, Apagado)
    GPIO.output(Deco1_B, Encendido)
    GPIO.output(Deco1_C, Encendido)
    GPIO.output(Deco1_D, Apagado)

def H2enLuzBajaApagado():
    GPIO.output(Deco1_A, Apagado)
    GPIO.output(Deco1_B, Encendido)
    GPIO.output(Deco1_C, Encendido)
    GPIO.output(Deco1_D, Encendido)

#-------------------------------------------

def H3enLuzAltaEncendido():
    GPIO.output(Deco1_A, Encendido)
    GPIO.output(Deco1_B, Apagado)
    GPIO.output(Deco1_C, Apagado)
    GPIO.output(Deco1_D, Apagado)


def H3enLuzAltaApagado():
    GPIO.output(Deco1_A, Encendido)
    GPIO.output(Deco1_B, Apagado)
    GPIO.output(Deco1_C, Apagado)
    GPIO.output(Deco1_D, Encendido)

def H3enLuzBajaEncendido():
    GPIO.output(Deco1_A, Encendido)
    GPIO.output(Deco1_B, Apagado)
    GPIO.output(Deco1_C, Encendido)
    GPIO.output(Deco1_D, Apagado)

def H3enLuzBajaApagado():
    GPIO.output(Deco1_A, Encendido)
    GPIO.output(Deco1_B, Apagado)
    GPIO.output(Deco1_C, Encendido)
    GPIO.output(Deco1_D, Encendido)

#-------------------------------------------

def H4enLuzAltaEncendido():
    GPIO.output(Deco1_A, Encendido)
    GPIO.output(Deco1_B, Encendido)
    GPIO.output(Deco1_C, Apagado)
    GPIO.output(Deco1_D, Apagado)


def H4enLuzAltaApagado():
    GPIO.output(Deco1_A, Encendido)
    GPIO.output(Deco1_B, Encendido)
    GPIO.output(Deco1_C, Apagado)
    GPIO.output(Deco1_D, Encendido)

def H4enLuzBajaEncendido():
    GPIO.output(Deco1_A, Encendido)
    GPIO.output(Deco1_B, Encendido)
    GPIO.output(Deco1_C, Encendido)
    GPIO.output(Deco1_D, Apagado)

def H4enLuzBajaApagado():
    GPIO.output(Deco1_A, Encendido)
    GPIO.output(Deco1_B, Encendido)
    GPIO.output(Deco1_C, Encendido)
    GPIO.output(Deco1_D, Encendido)

#-------------------------------------------
#-------------------------------------------
#-------------------------------------------
#-------------------------------------------

def H5enLuzAltaEncendido():
    GPIO.output(Deco2_A, Apagado)
    GPIO.output(Deco2_B, Apagado)
    GPIO.output(Deco2_C, Apagado)
    GPIO.output(Deco2_D, Apagado)

def H5enLuzAltaApagado():
    GPIO.output(Deco2_A, Apagado)
    GPIO.output(Deco2_B, Apagado)
    GPIO.output(Deco2_C, Apagado)
    GPIO.output(Deco2_D, Encendido)

def H5enLuzBajaEncendido():
    GPIO.output(Deco2_A, Apagado)
    GPIO.output(Deco2_B, Apagado)
    GPIO.output(Deco2_C, Encendido)
    GPIO.output(Deco2_D, Apagado)

def H5enLuzBajaApagado():
    GPIO.output(Deco2_A, Apagado)
    GPIO.output(Deco2_B, Apagado)
    GPIO.output(Deco2_C, Encendido)
    GPIO.output(Deco2_D, Encendido)

#-------------------------------------------

def H6enLuzAltaEncendido():
    GPIO.output(Deco2_A, Apagado)
    GPIO.output(Deco2_B, Encendido)
    GPIO.output(Deco2_C, Apagado)
    GPIO.output(Deco2_D, Apagado)


def H6enLuzAltaApagado():
    GPIO.output(Deco2_A, Apagado)
    GPIO.output(Deco2_B, Encendido)
    GPIO.output(Deco2_C, Apagado)
    GPIO.output(Deco2_D, Encendido)

def H6enLuzBajaEncendido():
    GPIO.output(Deco2_A, Apagado)
    GPIO.output(Deco2_B, Encendido)
    GPIO.output(Deco2_C, Encendido)
    GPIO.output(Deco2_D, Apagado)

def H6enLuzBajaApagado():
    GPIO.output(Deco2_A, Apagado)
    GPIO.output(Deco2_B, Encendido)
    GPIO.output(Deco2_C, Encendido)
    GPIO.output(Deco2_D, Encendido)

#-------------------------------------------

def H7enLuzAltaEncendido():
    GPIO.output(Deco2_A, Encendido)
    GPIO.output(Deco2_B, Apagado)
    GPIO.output(Deco2_C, Apagado)
    GPIO.output(Deco2_D, Apagado)


def H7enLuzAltaApagado():
    GPIO.output(Deco2_A, Encendido)
    GPIO.output(Deco2_B, Apagado)
    GPIO.output(Deco2_C, Apagado)
    GPIO.output(Deco2_D, Encendido)

def H7enLuzBajaEncendido():
    GPIO.output(Deco2_A, Encendido)
    GPIO.output(Deco2_B, Apagado)
    GPIO.output(Deco2_C, Encendido)
    GPIO.output(Deco2_D, Apagado)

def H7enLuzBajaApagado():
    GPIO.output(Deco2_A, Encendido)
    GPIO.output(Deco2_B, Apagado)
    GPIO.output(Deco2_C, Encendido)
    GPIO.output(Deco2_D, Encendido)

#-------------------------------------------

def H8enLuzAltaEncendido():
    GPIO.output(Deco2_A, Encendido)
    GPIO.output(Deco2_B, Encendido)
    GPIO.output(Deco2_C, Apagado)
    GPIO.output(Deco2_D, Apagado)


def H8enLuzAltaApagado():
    GPIO.output(Deco2_A, Encendido)
    GPIO.output(Deco2_B, Encendido)
    GPIO.output(Deco2_C, Apagado)
    GPIO.output(Deco2_D, Encendido)

def H8enLuzBajaEncendido():
    GPIO.output(Deco2_A, Encendido)
    GPIO.output(Deco2_B, Encendido)
    GPIO.output(Deco2_C, Encendido)
    GPIO.output(Deco2_D, Apagado)

def H8enLuzBajaApagado():
    GPIO.output(Deco2_A, Encendido)
    GPIO.output(Deco2_B, Encendido)
    GPIO.output(Deco2_C, Encendido)
    GPIO.output(Deco2_D, Encendido)

#-------------------------------------------
#-------------------------------------------
#-------------------------------------------


def H9enLuzAltaEncendido():
    GPIO.output(Deco3_A, Apagado)
    GPIO.output(Deco3_B, Apagado)
    GPIO.output(Deco3_C, Apagado)
    GPIO.output(Deco3_D, Apagado)

def H9enLuzAltaApagado():
    GPIO.output(Deco3_A, Apagado)
    GPIO.output(Deco3_B, Apagado)
    GPIO.output(Deco3_C, Apagado)
    GPIO.output(Deco3_D, Encendido)

def H9enLuzBajaEncendido():
    GPIO.output(Deco3_A, Apagado)
    GPIO.output(Deco3_B, Apagado)
    GPIO.output(Deco3_C, Encendido)
    GPIO.output(Deco3_D, Apagado)

def H9enLuzBajaApagado():
    GPIO.output(Deco3_A, Apagado)
    GPIO.output(Deco3_B, Apagado)
    GPIO.output(Deco3_C, Encendido)
    GPIO.output(Deco3_D, Encendido)

#-------------------------------------------

def H10enLuzAltaEncendido():
    GPIO.output(Deco3_A, Apagado)
    GPIO.output(Deco3_B, Encendido)
    GPIO.output(Deco3_C, Apagado)
    GPIO.output(Deco3_D, Apagado)


def H10enLuzAltaApagado():
    GPIO.output(Deco3_A, Apagado)
    GPIO.output(Deco3_B, Encendido)
    GPIO.output(Deco3_C, Apagado)
    GPIO.output(Deco3_D, Encendido)

def H10enLuzBajaEncendido():
    GPIO.output(Deco3_A, Apagado)
    GPIO.output(Deco3_B, Encendido)
    GPIO.output(Deco3_C, Encendido)
    GPIO.output(Deco3_D, Apagado)

def H10enLuzBajaApagado():
    GPIO.output(Deco3_A, Apagado)
    GPIO.output(Deco3_B, Encendido)
    GPIO.output(Deco3_C, Encendido)
    GPIO.output(Deco3_D, Encendido)

#-------------------------------------------

def H11enLuzAltaEncendido():
    GPIO.output(Deco3_A, Encendido)
    GPIO.output(Deco3_B, Apagado)
    GPIO.output(Deco3_C, Apagado)
    GPIO.output(Deco3_D, Apagado)


def H11enLuzAltaApagado():
    GPIO.output(Deco3_A, Encendido)
    GPIO.output(Deco3_B, Apagado)
    GPIO.output(Deco3_C, Apagado)
    GPIO.output(Deco3_D, Encendido)

def H11enLuzBajaEncendido():
    GPIO.output(Deco3_A, Encendido)
    GPIO.output(Deco3_B, Apagado)
    GPIO.output(Deco3_C, Encendido)
    GPIO.output(Deco3_D, Apagado)

def H11enLuzBajaApagado():
    GPIO.output(Deco3_A, Encendido)
    GPIO.output(Deco3_B, Apagado)
    GPIO.output(Deco3_C, Encendido)
    GPIO.output(Deco3_D, Encendido)

#-------------------------------------------

def H12enLuzAltaEncendido():
    GPIO.output(Deco3_A, Encendido)
    GPIO.output(Deco3_B, Encendido)
    GPIO.output(Deco3_C, Apagado)
    GPIO.output(Deco3_D, Apagado)


def H12enLuzAltaApagado():
    GPIO.output(Deco3_A, Encendido)
    GPIO.output(Deco3_B, Encendido)
    GPIO.output(Deco3_C, Apagado)
    GPIO.output(Deco3_D, Encendido)

def H12enLuzBajaEncendido():
    GPIO.output(Deco3_A, Encendido)
    GPIO.output(Deco3_B, Encendido)
    GPIO.output(Deco3_C, Encendido)
    GPIO.output(Deco3_D, Apagado)

def H12enLuzBajaApagado():
    GPIO.output(Deco3_A, Encendido)
    GPIO.output(Deco3_B, Encendido)
    GPIO.output(Deco3_C, Encendido)
    GPIO.output(Deco3_D, Encendido)



#-------------------------------------------
#-------------------------------------------
#-------------------------------------------

def fuente1Encendido():
    GPIO.output(F1, Encendido)


def fuente1Apagado():
    GPIO.output(F1, Apagado)



def fuente2Encendido():
    GPIO.output(F2, Encendido)

def fuente2Apagado():
    GPIO.output(F2, Apagado)



def fuente3Encendido():
    GPIO.output(F3, Encendido)


def fuente3Apagado():
    GPIO.output(F3, Apagado)























