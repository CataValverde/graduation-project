
import pickle



class ListCategories():
    def __init__(self):
        self.Categories=[]

    def Save(self):
        with open('ListCategories.categ', 'wb') as File:
            pickle.dump(self.Categories, File)

    def Load(self):
        with open('ListCategories.categ', 'rb') as File:
            self.Categories = pickle.load(File)

    def AddCategory(self, Name):
        self.Categories.append(Category(Name,len(self.Categories)))
        self.Save()

    def DeleteCategory(self,ListPosition):
        self.Categories.pop(ListPosition)

        for k in range(len(self.Categories)):
            self.Categories[k].ListPosition=k

        self.Save()

    def AddSubject(self,CategoryPosition, Name, Book, Page, InitialExerNum, FinalExerNum):
        self.Categories[CategoryPosition].AddSubsection( Name, Book, Page, InitialExerNum,FinalExerNum)
        self.Save()

    def DeleteSubject(self,CategoryPosition, SubjectPosition):
        self.Categories[CategoryPosition].DeleteSubsection(SubjectPosition)
        self.Save()

    def GetSubjectInformation(self, CategoryNumber, SubjectNumber):
        CategoryName=self.Categories[CategoryNumber].Name
        Name=self.Categories[CategoryNumber].Subsections[SubjectNumber].Name
        Book=self.Categories[CategoryNumber].Subsections[SubjectNumber].Book
        Page=self.Categories[CategoryNumber].Subsections[SubjectNumber].Page
        return CategoryName,Name,Book, Page

    def DictionaryGetValue(self, CategoryNumber,SubjectNumber,key):
        return self.Categories[CategoryNumber].Subsections[SubjectNumber].ExercisesDictio[key]

    def DictionaryGetKeys(self, CategoryNumber,SubjectNumber):
        return self.Categories[CategoryNumber].Subsections[SubjectNumber].ExercisesDictio.keys()

    def ChangeKeyValue(self, CategoryNumber,SubjectNumber,key):
        value=self.Categories[CategoryNumber].Subsections[SubjectNumber].ExercisesDictio[key]
        if value==False:
            self.Categories[CategoryNumber].Subsections[SubjectNumber].ExercisesDictio[key]=True
        else:
            self.Categories[CategoryNumber].Subsections[SubjectNumber].ExercisesDictio[key]=False

        self.Save()









class Category():
    def __init__(self, Name, ListPosition):
        self.Name=Name
        self.ListPosition=ListPosition
        self.Subsections=[]

    def AddSubsection(self, Name, Book, Page, InitialExerNum,FinalExerNum):
        self.Subsections.append(Subsection(Name,Book,Page,InitialExerNum,FinalExerNum, len(self.Subsections)))

    def DeleteSubsection(self, ListPosition):
        self.Subsections.pop(ListPosition)
        counter=0
        for SubSec in self.Subsections:
            SubSec.ListPosition=counter
            counter+=1







class Subsection():
    def __init__(self, Name, Book, Page, InitialExerNum, FinalExerNum, ListPosition):
        self.Name=Name
        self.Book=Book
        self.Page=Page
        self.InitialExerNum=InitialExerNum
        self.FinalExerNum=FinalExerNum
        self.NumOfExer=FinalExerNum-InitialExerNum
        self.ListPosition=ListPosition

        self.ExercisesDictio={}

        for k in range(InitialExerNum,FinalExerNum+1):
            self.ExercisesDictio[k]=False





