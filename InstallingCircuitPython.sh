#!/bin/bash

#Instalando CircuitPython
#https://learn.adafruit.com/circuitpython-on-raspberrypi-linux/installing-circuitpython-on-raspberry-pi

sudo apt-get update

sudo apt-get upgrade

sudo pip3 install --upgrade setuptools

#Configuring i2c

sudo apt-get install -y python-smbus

sudo apt-get install -y i2c-tools

sudo raspi-config

#Enabling i2c

#https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c

#Go to Interfacing Options

#then I2C

#Enable!

sudo reboot

#Once i2c is enabled


pip3 install RPI.GPIO

pip3 install adafruit-blinka